﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

/*
	TODO: 
	- Remove duplication.  There's duplication in script names.
	- There's duplication that could be remove with dictionaries.
	- Change name of Update UI button to something that better reflects its usage.
	- Insert error logic.
	- Split logic into different functions.  Everything in update is a total mess.
*/

/*
Choose powerups with numbers.  Debug keys.
Use powerups with mouse button.
*/
public class LevelLoop : MonoBehaviour {
	// Script with counters.
	private PowerUps powerUpsScript;
	// Used to discriminate behaviour between spherical and flat levels.  y coordinate.
    public bool isFlat = true; 
	
	public enum PowerUpType {
		Neutralize,
		Freeze,
		Walls,
		SuperLove,
        ChangeSpeed,
        ChangeDirection,
        Sacrifice
	};

	public PowerUpType powerUpType;

	/*
		pre: Existing PowerUps script.
	*/
	void Awake() {
		powerUpsScript = gameObject.GetComponent<PowerUps> ();
	}

	void Update () {
		ProcessInput ();
	}

	/*
	pre: Existing GameObject with buttonName. Toggle component.
	post: turns button (Toggle) on.
	*/
	void UpdateUIButton (string buttonName){
		GameObject go = GameObject.Find (buttonName);
		Toggle button = go.GetComponent <Toggle> ();
		button.isOn = true;
	}

	/*
	pre: Audio resources.  Renderer components in children.  Materials.  Gorro object in proper
	hierarchy of selected object.  Existing SimpleMove & RotateAround & DestroyOnTriggerEnter script.  
	Existing wall prefabs
	post: Load new scenes.  Apply powerups with sounds.(DEBUG)
	*/
	void ProcessInput() {
		if (Input.GetKeyDown (KeyCode.Space)) {
			string sceneName = SceneManager.GetActiveScene().name;
			SceneManager.LoadScene (sceneName, LoadSceneMode.Single);
            GameManager.i.ResetTimeAndDeaths();
		}
		if (Input.GetKeyDown (KeyCode.M)) {
			int sceneNumber = SceneManager.GetActiveScene ().buildIndex;
			int nextScene = sceneNumber + 1;
			SceneManager.LoadScene (nextScene);
			GameManager.i.ResetTimeAndDeaths();
		}
		if (Input.GetKeyDown (KeyCode.N)) {
			int sceneNumber = SceneManager.GetActiveScene ().buildIndex;
			int nextScene = sceneNumber - 1;
			SceneManager.LoadScene (nextScene);
			GameManager.i.ResetTimeAndDeaths();
		}
		if (Input.GetKeyDown ("1")) {
			powerUpType = PowerUpType.Neutralize;
			UpdateUIButton ("Neutralize");
		}
		else if (Input.GetKeyDown ("2")) {
			powerUpType = PowerUpType.Freeze;
			UpdateUIButton ("Freeze");
		}
        else if (Input.GetKeyDown("3"))
        {
            powerUpType = PowerUpType.Walls;
			UpdateUIButton ("Walls");
        }
        else if (Input.GetKeyDown("4"))
        {
            powerUpType = PowerUpType.SuperLove;
			UpdateUIButton ("SuperLove");
        }
        else if (Input.GetKeyDown("5"))
        {
            powerUpType = PowerUpType.ChangeSpeed;
        }
        else if (Input.GetKeyDown("6"))
        {
            powerUpType = PowerUpType.ChangeDirection;
        }
        else if (Input.GetKeyDown("7"))
        {
            powerUpType = PowerUpType.Sacrifice;
        }


        if (Input.GetMouseButtonDown (0)) { //left lcik

			RaycastHit hit;
            Camera cam = Camera.main;
            Debug.Log("mouse " + Input.mousePosition.ToString());
            foreach (Camera c in Camera.allCameras) {
                if(c.pixelRect.Contains(Input.mousePosition)) {
                    cam = c;
                }
            }
            Ray ray = cam.ScreenPointToRay(Input.mousePosition); 
			Vector3 from = cam.transform.position;
			Debug.DrawRay (from, ray.direction * 1000.0f, Color.yellow, 10.0f);
			if (Physics.Raycast (ray, out hit, 100.0f)) {
				GameObject obj = hit.collider.gameObject;

                if (obj.tag.Contains("Faction"))
                { // don't neutralize, freeze, etc walls, floors, or other objects.
                    if (powerUpType == PowerUpType.Neutralize)
                    {
                        if (powerUpsScript.neutralize > 0)
                        {
							Renderer rend = obj.GetComponentInChildren<Renderer>();
                            Material newMat = Resources.Load("FactionNeutralMaterial", typeof(Material)) as Material;
                            obj.tag = "FactionNeutral";
                            powerUpsScript.neutralize -= 1;
                            AudioSource source = cam.GetComponent<AudioSource>();
                            source.clip = Resources.Load("audio/gorro") as AudioClip;
                            if (source)
                            {
                                source.PlayOneShot(source.clip);
                            }


                            Transform gorro = obj.transform.FindChild("agent/gorro");
                            if (gorro)
                            {
								Debug.Log (gorro.name);
                                Debug.Log("Gorro found");
                                Destroy(gorro.gameObject);
                            }
                            else { Debug.Log("Crap"); }

                        }
                    }
                    else if (powerUpType == PowerUpType.Freeze)
                    {

                        if (powerUpsScript.freeze > 0)
                        {
                            SimpleMove simple = obj.GetComponent<SimpleMove>();
                            if(simple) simple.speedDirection = new Vector3(0.0f, 0.0f, 0.0f);
                            RotateAround rotateScript = obj.GetComponentInParent<RotateAround>();
                            if (rotateScript) rotateScript.personalSpeedModifier = 0.0f;
                            powerUpsScript.freeze -= 1;
							AudioSource source = cam.GetComponent<AudioSource>();
							source.clip = Resources.Load("audio/congelacion") as AudioClip;
							if (source)
							{
								source.PlayOneShot(source.clip);
							}
                        }
                    }
                    else if (powerUpType == PowerUpType.ChangeDirection)
                    {

                        if (powerUpsScript.changeDirection > 0)
                        {
                            SimpleMove simple = obj.GetComponent<SimpleMove>();
                            if (simple) simple.RandomizeSpeed();
                            RotateAround rotateScript = obj.GetComponentInParent<RotateAround>();
                            if (rotateScript) rotateScript.initialAxis = new Vector3(Random.value, Random.value, Random.value);
                            powerUpsScript.changeDirection -= 1;
							AudioSource source = cam.GetComponentInChildren<AudioSource>();
							source.clip = Resources.Load("audio/cambio_direccion") as AudioClip;
							if (source)
							{
								source.PlayOneShot(source.clip, 1.0f);
							}
                        }
                    }
                    else if (powerUpType == PowerUpType.ChangeSpeed)
                    {

                        if (powerUpsScript.changeSpeed > 0)
                        {
                            SimpleMove simple = obj.GetComponent<SimpleMove>();
                            if (simple) simple.personalSpeedModifier = Random.Range(0.2f, 3.0f);
                            RotateAround rotateScript = obj.GetComponentInParent<RotateAround>();
                            if (rotateScript) rotateScript.personalSpeedModifier = Random.Range(0.2f, 3.0f);
                            powerUpsScript.changeSpeed -= 1;
							AudioSource source = cam.GetComponentInChildren<AudioSource>();
							source.clip = Resources.Load("audio/cambio_velocidad") as AudioClip;
							if (source)
							{
								source.PlayOneShot(source.clip, 1.0f);
							}
                        }
                    }
                    else if (powerUpType == PowerUpType.Sacrifice)
                    {

                        if (powerUpsScript.sacrifice > 0)
                        {
                            DestroyOnTriggerEnter destroyScript = obj.GetComponentInParent<DestroyOnTriggerEnter>();
                            Debug.Log("obj name: " + obj.name);
                            if (destroyScript)
                            {
                                destroyScript.KillAgent();
                                powerUpsScript.sacrifice -= 1;
                            }
                        }

						AudioSource source = cam.GetComponentInChildren<AudioSource>();
						source.clip = Resources.Load("audio/sacrificio") as AudioClip;
						if (source)
						{
							source.PlayOneShot(source.clip, 1.0f);
						}
                    }

                    else if (powerUpType == PowerUpType.SuperLove)
                    {

                        if (powerUpsScript.superLove > 0)
                        {
                            Renderer rend = obj.GetComponentInChildren<Renderer>();
                            Material newMat = Resources.Load("FactionLoveMaterial", typeof(Material)) as Material;
                            rend.material = newMat;
                            obj.tag = "FactionLove";
                            powerUpsScript.superLove -= 1;
                            Transform gorro = obj.transform.FindChild("Tiptio/gorro");
                            if (gorro)
                            {
                                Debug.Log("Gorro found");
                                Destroy(gorro.gameObject);
                            }
							AudioSource source = cam.GetComponentInChildren<AudioSource>();
							source.clip = Resources.Load("audio/amor") as AudioClip;
							if (source)
							{
								source.PlayOneShot(source.clip);
							}
                        }
                    }
                }

                if (powerUpType == PowerUpType.Walls)
                {
                    if (powerUpsScript.walls > 0)
                    {
                        GameObject haha = Instantiate(Resources.Load("Prefabs/Wall") as GameObject);
                        float y = 0.0f;
						if (!isFlat) y = hit.point.y + 0.5f;
						if (obj.name == "Floor") y = hit.point.y + 0.5f;
                        haha.transform.position = new Vector3(hit.point.x, y, hit.point.z);
                        
                        powerUpsScript.walls -= 1;
                        if(obj.tag.Contains("Faction"))
                        {
                            Destroy(obj);
                            GameManager.i.deathsSoFar += 1;
                            AudioSource source = cam.GetComponent<AudioSource>();
                            source.clip = Resources.Load("audio/choque") as AudioClip;
                            if (source)
                            {
                                source.PlayOneShot(source.clip);
                            }   
                        }
                        else
                        {
                            AudioSource source = cam.GetComponent<AudioSource>();
                            source.clip = Resources.Load("audio/muro") as AudioClip;
                            if (source)
                            {
                                source.PlayOneShot(source.clip);
                            }

                        }
                    }
                }
                Debug.Log ("Hit " + obj.tag);
			}


            
        }
        
    }
}
