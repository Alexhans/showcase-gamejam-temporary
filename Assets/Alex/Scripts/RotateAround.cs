﻿using UnityEngine;
using System.Collections;

/*
TODO:
	- Remove Magic numbers 5, 15.0f.
	- Why are there 2 RotateAround calls? 
*/
/*
Handle simple movement in Spheric levels.  
Basically uses an origin point, speed and rotation axis to RotateAround.
The initialAxis of rotation is randomized providing it a "randomized direction".
*/
public class RotateAround : MonoBehaviour {
	public  Transform planet;	

	public Vector3 initialAxis;
	
    [HideInInspector]
	public float personalSpeedModifier = 1.0f;	
	
	/*
		post: initial Random axis of rotation is set.
	*/
	void Start () {
        float x = Random.value;
        float y = Random.value;
        float z = Random.value;
        personalSpeedModifier = 1.0f;
        Vector3 axis = new Vector3(x, y, z);
        initialAxis = axis;
        transform.RotateAround(Vector3.zero, axis * Random.Range(0.0f, 15.0f), Random.Range(0,360));  // not sure why this is here.
    }

	/*
		pre: GameManager.i.agentSpeed exists
		post: objects is rotated around the "planet" origin
	*/
	void Update () {
        transform.RotateAround(Vector3.zero, initialAxis, GameManager.i.agentSpeed * 5 * personalSpeedModifier * Time.deltaTime);	
    }
}
