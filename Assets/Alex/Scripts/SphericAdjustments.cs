﻿using UnityEngine;
using System.Collections;

/*
	TODO: Remove debug code attached to keys 'a' and 's'
	Remove debug code.
*/

/*
	Class adjusts any object so that it's correctly standing on the surface.  
	It's specially important because "cluster spawns" will spawn object anywhere in a sphere and, this way, 
	you can make sure that sphere of "spawning" is adjusted to the surface it covers.
*/
public class SphericAdjustments : MonoBehaviour {
    public Transform sphere;
    
    public float radiusOfPlanet;
    public float offsetDueToOwnSize;
    
	void Update () {
        if (Input.GetKeyDown("a"))
        {
            AdjustToSphere();
        }
        if (Input.GetKeyDown("s"))
        {
            AlignToSphere();
        }
    }

	/*
		post: modifies transform so that object's "floor" is on top of surface.
		
	*/
    public void AdjustToSphere()
    {
        Debug.Log("AdjustToSphere " + gameObject.name);

        Vector3 origin = sphere.transform.position;
        Vector3 dest = gameObject.transform.position;
        Vector3 dir = origin - dest;

        Vector3 norm = dir.normalized;

        Debug.Log("dir: " + dir.ToString());
        Debug.Log("norm: " + norm.ToString());

        // put new pos  
        gameObject.transform.position = norm * -(radiusOfPlanet + offsetDueToOwnSize);
        Debug.Log("(radiusOfPlanet + offsetDueToOwnSize): " + (radiusOfPlanet + offsetDueToOwnSize).ToString());
    }
	
	/*
		post: modifies object transform so that it's placed properly 
		("height" axis of object is normal to sphere)
	*/
    public void AlignToSphere()
    {
        Debug.Log("AlignToSphere");
        gameObject.transform.LookAt(sphere);
            
    }
}
