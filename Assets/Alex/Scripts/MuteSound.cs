﻿using UnityEngine;
using System.Collections;

/*
	TODO: Rename MuteSound because it's actually a toggle.
*/

public class MuteSound : MonoBehaviour {
    public void Toggle() {
        AudioListener.pause = !AudioListener.pause;     
    }
}
