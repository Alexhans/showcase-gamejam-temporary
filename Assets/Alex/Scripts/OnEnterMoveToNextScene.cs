﻿using UnityEngine;

/*
TODO: delete or change to something that's not easy to discover.
Debug class that intercepts the Enter key to load the next scene if there is one.
*/
public class OnEnterMoveToNextScene : MonoBehaviour {
	/*
		post: Load next scene.
	*/
	void Update () {
        if (Input.GetKey(KeyCode.Return))
        {
            int index = UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex;
            UnityEngine.SceneManagement.SceneManager.LoadScene(index + 1);
            Debug.Log("test");
        }
    }
}
