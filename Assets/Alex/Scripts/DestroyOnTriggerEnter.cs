﻿using UnityEngine;
using System.Collections;

/*
TODO: Understand why it didn't work when we tried OnCollisionEnter.
- The name of the class doesn't reflect that it also handles collisions with walls that change direction,
and every other effect.  Rename class.
- remove literals 
- rethink random direction hack.
- split into functions
*/

/*
Handle collisions with Walls, FactionLove effect and things that don't kill you.  
Everything kils you by default.

pre: SimpleMove Script. FactionLoveMaterial. Renderer in this object and collider.
post: changes speed and position OR material OR KillsAgent.
*/
public class DestroyOnTriggerEnter : MonoBehaviour {
	void OnTriggerEnter(Collider other) {
		if (other.tag == "Wall"){
			SimpleMove simple = gameObject.GetComponent<SimpleMove> ();
			// Translation hack in case the first speed is in a wrong direction (similar to original direction)
			gameObject.transform.Translate (-simple.speedDirection * 0.1f); 
			if(Random.value > .3 )	simple.speedDirection.z *= -1;
			if(Random.value > .3 )	simple.speedDirection.x *= -1;
			return;
		}

		if (other.tag == "FactionNeutral" || gameObject.tag == "FactionNeutral" || other.tag == "Floor")
			return; // si es neutral no hace nada.
		if(other.tag == "FactionLove" || gameObject.tag == "FactionLove")
        {
            other.tag = "FactionLove";
            gameObject.tag = "FactionLove";
            Material newMat = Resources.Load("FactionLoveMaterial", typeof(Material)) as Material;
			Renderer rend = other.GetComponentInChildren<Renderer>();
            rend.material = newMat;
			Renderer rend2 = gameObject.GetComponentInChildren<Renderer>();
            rend2.material = newMat;
        }
		if (this.tag != other.gameObject.tag) {
            KillAgent();
		}
	}

	/*
	pre: AudioSource in camera.  sound file "audio/muerte"
	post: 1 more death. Destroy this gameobject. play sound.
	*/
    public void KillAgent() {
        Debug.Log("Different tag");
        GameManager.i.deathsSoFar += 1;
        Destroy(gameObject);
        AudioSource source = Camera.main.GetComponent<AudioSource>();
        source.clip = Resources.Load("audio/muerte") as AudioClip;
        if (source)
        {
            source.Play();
        }
    }
	
	/* TODO: Delete unused code after understanding why it didn't work.
	void OnCollisionEnter(Collision col){
		Debug.Log ("on collsion Enter");
		SimpleMove simple = gameObject.GetComponent<SimpleMove> ();
		foreach (ContactPoint contact in col){
			simple.speedDirection = Vector3.Reflect (simple.speedDirection, contact.normal);
		}
	}*/
}
