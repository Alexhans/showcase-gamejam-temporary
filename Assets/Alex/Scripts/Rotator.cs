﻿using UnityEngine;
using System.Collections;

/*UNUSED: DELETE:
	Originally used to rotate the world sphere around but it was considered unfeasible (although easy on the eyes)
	because it would make it harder to program relative spheric movement and because it would be more confusing to the player.
	
	Current solution shows both sides of the sphere.
*/
public class Rotator : MonoBehaviour {
	void Update () {
        float speed = 30.0f;
        transform.Rotate(Vector3.left + Vector3.up, speed * Time.deltaTime);
    }
}
