﻿using UnityEngine;
using System.Collections;

/*
	Used in tutorial scene.  
*/
public class GoForward : MonoBehaviour {
	public Vector3 dir = new Vector3(0.0f, 0.0f, 0.0f);
	
	void Update () {
		gameObject.transform.Translate (dir * Time.deltaTime);
	}
}

