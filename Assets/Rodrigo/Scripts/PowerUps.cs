﻿using UnityEngine;
using System.Collections;

/*
Power Up counters.
*/

public class PowerUps : MonoBehaviour {
	public int freeze = 0;
	public int neutralize = 0;
	public int walls = 0;
	public int superLove = 0;
    public int changeSpeed = 0;
    public int changeDirection = 0;
    public int sacrifice = 0;
}
