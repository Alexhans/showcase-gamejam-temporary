﻿using UnityEngine;
using System.Collections;

/*
TODO: Rename sTime to something more clear like startTime or initialTime.
Take logic out of update and into its own function.
*/
/*
Used in the game menu too move the camera back and forth with ease.  
*/
public class CameraMoveLoop : MonoBehaviour {

	public float speed;
	public bool direction;
	public float time;
	private float sTime;

	void Start () {
		sTime = time;
	
	}

	/*
		post: translates object in one direction or the other.
	*/
	void Update () {
		time -= Time.deltaTime;

		if (direction) {
			if (time > 0) transform.Translate (0, 0, speed * Time.deltaTime);
			if (time <= 0){
				time = sTime;
				direction = false;
			}
				
		} else {
			if (time > 0) transform.Translate (0, 0, speed * Time.deltaTime * -1);
			if (time <= 0) {
				time = sTime;
				direction = true;
			}
		}

	}
}
