﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/*
	TODO: LoveCounter, FreezeCounter, NeutralizeCounter and Wall Counter share the same code
	Replaced them with one function and an enum to choose the appropriate type?
*/

public class NeutralizeCounter : MonoBehaviour {
	private GameObject gO;
	private PowerUps pU;
	private Text text;

	/*
	pre: PowerUps, LevelSettings object, Text.
	*/
	void Awake(){
		gO = GameObject.Find ("LevelSettings");
		pU = gO.GetComponent <PowerUps> ();
		text = GetComponent <Text> ();
	}

	void Update () {
		text.text = pU.neutralize.ToString ();
	}
}
