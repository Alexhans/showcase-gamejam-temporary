﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

/* TODO:
	- Rename single variable name i to instance or something easier to search.
	Better yet, make it a static function that returns the variable?
	- Why are there public attributes with serialize field?  Either make them private
	or remove the flag
	- rename startDelay to initialDelay for consistency
	- Rename enum watingForGame to something more accurate that reflects its usage 
	for the "end level" pause.
	- Remove code duplication.
	- Remove magic numbers.
*/
/*
	"Static" Class (can be accesed from anywhere an of which there's an instance)
	
	
*/
public class GameManager : MonoBehaviour {

	public static GameManager i;

	public float startDelay;

	[SerializeField]
	public int timeToSurvive = 60;

	// Tolerance of lives that can be wasted.
	public 	int lives; 

	[Range(0.2f, 10.0f)]
	public float agentSpeed;

	[SerializeField]
	private float[] percentageOfReligion;

	[SerializeField]
	private float[] powerUps;

	[HideInInspector]
	public float timeLeft;

	public enum Status{
		isPlaying,
		isWating,
		watingForGame  // TODO what is the difference between waitingforgame and iswaiting?
	};

	[HideInInspector]
	public Status status;

	public int deathsSoFar;

	/*
		
	*/
	void Awake () {
		if (i == null) {
			i = this;
			// Makes the object target not be destroyed automatically when loading a new scene.
			DontDestroyOnLoad (gameObject);
		} else {
			Destroy (gameObject);  // TODO Why is this here?  How does it work?
		}
	}
	
	void Start () { 
		status = Status.isWating;
        ResetTimeAndDeaths();  
    }

	/*
		post: initial settings restored (time, deaths)
	*/
    public void ResetTimeAndDeaths()
    {
        timeLeft = timeToSurvive;
        deathsSoFar = 0;
        status = Status.isWating;
    }
	
	/*
		Manages the status of the game.  Whether it ends via 
		end of time (win), too many deaths (lose) or waits 
		for the action to begin.
		
		TODO: Extract function.  use functions like timeFinished() instead 
		of timeLeft <= 0.
		
	*/
	void Update () {
		if (status == Status.isPlaying)
		{
			timeLeft -= Time.deltaTime;
			if (timeLeft <= 0)
			{
				status = Status.watingForGame;
				StartCoroutine(EndScene (true));
			}
			if (deathsSoFar > lives)
			{

				status = Status.watingForGame;
				StartCoroutine(EndScene (false));
			}
		}
		if (status == Status.isWating)
		{
			StartCoroutine (StartDelay ());
		}
		if (status == Status.watingForGame){
			return;
		}
	}
/////////////////////////////////////////////////////////////////
	// Functions for couroutines.
/////////////////////////////////////////////////////////////////

	/*
		Initial delay at the beginning of a level to give 
		the players a time to observe the level and react.
	*/
	IEnumerator StartDelay(){
		yield return new WaitForSeconds (startDelay);
		status = Status.isPlaying;
	}

	/*
		TODO: Remove repeated code.
		Loads either the win or lose scene after 
		waiting an appropriate amount of time.
	*/
	IEnumerator EndScene(bool win){
		yield return new WaitForSeconds (1.5f);

		if (win) {
			SceneSwitcher.lastScene = SceneManager.GetActiveScene ().name;
			SceneManager.LoadScene ("Win", LoadSceneMode.Single);
			Destroy (gameObject);
		} 
		if (!win) { // TODO else
			SceneSwitcher.lastScene = SceneManager.GetActiveScene().name;
			SceneManager.LoadScene ("Lose", LoadSceneMode.Single);
			Destroy (gameObject);
		}




	}


}
