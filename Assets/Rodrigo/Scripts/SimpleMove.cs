﻿using UnityEngine;
using System.Collections;

/*
TODO:  GameManager.i.agentSpeed should be agentSpeedModifier for consistency and actual usage.  
It's not used in isolation.
*/
/*
Handle simple movement in Flat levels.  Direction and speed on the XZ plane.
*/
public class SimpleMove : MonoBehaviour {
	public Vector3 speedDirection;
	public float personalSpeedModifier = 1.0f;   // Used to be able to modify speed values to unique agents. 
	
	/*
		post: Newly randomized speed.
	*/
	void Start () {
		RandomizeSpeed ();
	}
	
	/*
	pre: Requires a "global" agentSpeed modifier 
	post: Translates object to new calculated position based on speed.
	*/
	void Update () {
        float speed = GameManager.i.agentSpeed * personalSpeedModifier;
		if (GameManager.i.status == GameManager.Status.isPlaying) {
			transform.Translate (speedDirection.x * speed *  Time.deltaTime, 0, speedDirection.z * speed *  Time.deltaTime);
		}
	}

	/*
	post: Changes speed direction in both x and z (flat).  Remember y is height in Unity coordinates.
	*/
	public void RandomizeSpeed(){
		speedDirection = new Vector3 (Random.value, 0, Random.value);
	}
}
