﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
	TODO: extract function from start.
	- Rethink flat/spherical logic and required "input" in the level editor.
	- Error code in the inspector (if spherical, need planet, for example)
	- Prevent user from making mistakes.
*/
/*
	Provides the ability to spawn objects.
	Supports (ugly) both Flat and Spherical worlds.
	You need to provide the transform of the planet object (to get the origin around which to rotate)
	You need to provide the prefab that will be spawned and the amount.
	Range is also specified.
	
	It positions objects randomly along the surface.
*/
public class Spawn : MonoBehaviour {
	public bool isFlatWorld = true;
	public  Transform planet;	
	public int range;
	public GameObject faction;
	public int amount;

	private Vector3 pos;
	private Quaternion rot;

	/*
		pre: requires specific material.  Requires SphericAdjustments script.  Requires a planet if 
		it's a sphere.  
	*/
	void Start () {
		for (int i = 0; i < amount; i++){
			pos.x = Random.Range (-range, range) + transform.position.x;
			pos.z = Random.Range (-range, range) + transform.position.z;
			// y (height) doesn't matter if it's a flat world.
            if (!isFlatWorld) { pos.y = Random.Range(-range, range) + transform.position.y; }
            rot = Quaternion.Euler( 0 , Random.Range(0, 360) , 0);
			GameObject newObject = Instantiate (faction, pos, rot) as GameObject;
			// TODO: probably extract this function that tags all created objects.
			newObject.tag = this.tag;
            foreach (Transform t in newObject.transform)
            {
                t.gameObject.tag = this.tag;
            }
            // Warning, the material must be named as "Faction Name"+Material and be in the resources folder.
            Renderer rend = newObject.GetComponentInChildren<Renderer>();
			Material newMat = Resources.Load(newObject.tag + "Material", typeof(Material)) as Material;
			//rend.material = newMat;

			if (!isFlatWorld) {
                SphericAdjustments adj = newObject.GetComponent<SphericAdjustments>();
                adj.sphere = planet;

                adj.AdjustToSphere();
                adj.AlignToSphere();
			}
		}
	}

	/*
		Used to easily showcase the range of the "spawning"
	*/
	void  OnDrawGizmos () {
			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere (transform.position, range);
		}
}
