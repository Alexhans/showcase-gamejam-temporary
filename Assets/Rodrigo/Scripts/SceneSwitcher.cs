﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

/*
	TODO: Remove code duplication.
*/
/*
	Handles loading of different scenes and quitting the game.
*/
public class SceneSwitcher : MonoBehaviour {

	public static string lastScene;

	public void LoadScene(string sceneName) {
		lastScene = SceneManager.GetActiveScene().name;
		SceneManager.LoadScene (sceneName, LoadSceneMode.Single);
	}

	public void LoadLastScene() {
		string last = lastScene;
		lastScene = SceneManager.GetActiveScene().name;
		SceneManager.LoadScene (last, LoadSceneMode.Single);
	}

	public void QuitGame (){
		Application.Quit ();
	}
}
