﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CountDown : MonoBehaviour {
	private Text textField;

	void Start () {
		textField = GetComponent<Text> ();
		InvokeRepeating ("Timer", 0, 1);
	}

	void Timer() {
		textField.text = GameManager.i.timeLeft.ToString ("00");
	}
	}

