﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/*
	TODO: change into for loop of an array or something.  Not hardcoded numbers.
	This class allows for the selection of different powerups in the UI.
*/
public class PowerUpSelector : MonoBehaviour {
	private GameObject gO;
	private LevelLoop level;

	/*
		pre: Existing LevelSettings object.  LevelLoop script.
		post: have handles for both objects to be able to select them accordingly. 
	*/
	void Awake(){
		gO = GameObject.Find ("LevelSettings");
		level = gO.GetComponent <LevelLoop> ();
	}
	
	public void PowerUpType (int num) {
		if (num == 1) level.powerUpType = LevelLoop.PowerUpType.Neutralize;
		if (num == 2) level.powerUpType = LevelLoop.PowerUpType.Freeze;
		if (num == 3) level.powerUpType = LevelLoop.PowerUpType.Walls;
		if (num == 4) level.powerUpType = LevelLoop.PowerUpType.SuperLove;
		if (num == 5) level.powerUpType = LevelLoop.PowerUpType.ChangeSpeed;
		if (num == 6) level.powerUpType = LevelLoop.PowerUpType.ChangeDirection;
        if (num == 7) level.powerUpType = LevelLoop.PowerUpType.Sacrifice;
    }

}
