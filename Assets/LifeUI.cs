﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LifeUI : MonoBehaviour {

	private Text text;
	void Start () {
		text = GetComponent <Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		text.text = (GameManager.i.lives - GameManager.i.deathsSoFar).ToString ("00");
	}
}
